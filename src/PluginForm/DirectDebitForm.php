<?php

namespace Drupal\commerce_slimpay\PluginForm;

use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;

/**
 * Iframe integration for direct debit payments.
 */
class DirectDebitForm extends PaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_mollie\Plugin\Commerce\PaymentGateway\DirectDebit $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();

    $iframe_code = $payment_gateway_plugin->getPaymentIframe($payment);

    $progress_bar = '<div class="progress"><div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div></div>';
    $form['extended-user-approval'] = [
      '#type' => 'inline_template',
      '#template' => '<div>' . $progress_bar . $iframe_code . '</div>',
      '#attached' => [
        'library' => ['commerce_slimpay/progress'],
      ],
    ];

    return $form;
  }

}
