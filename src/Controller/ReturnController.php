<?php

namespace Drupal\commerce_slimpay\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

/**
 * Middleware that redirects to cancel or completed checkout step.
 */
class ReturnController extends ControllerBase {

  /**
   * Redirects to the "return" checkout payment page.
   */
  public function success(Request $request, RouteMatchInterface $route_match) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');

    $return_url = Url::fromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE])->toString();

    return [
      '#type' => 'inline_template',
      '#template' => '<div class="ajax-progress ajax-progress-fullscreen">&nbsp;</div>',
      '#attached' => [
        'library' => ['commerce_slimpay/redirect'],
        'drupalSettings' => [
          'returnUrl' => $return_url,
        ],
      ],
    ];
  }

  /**
   * Redirects to the "cancel" checkout payment page.
   */
  public function cancel(Request $request, RouteMatchInterface $route_match) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $route_match->getParameter('commerce_order');

    $return_url = Url::fromRoute('commerce_payment.checkout.cancel', [
      'commerce_order' => $order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE])->toString();

    return [
      '#type' => 'inline_template',
      '#template' => '<div class="ajax-progress ajax-progress-fullscreen">&nbsp;</div>',
      '#attached' => [
        'library' => ['commerce_slimpay/redirect'],
        'drupalSettings' => [
          'returnUrl' => $return_url,
        ],
      ],
    ];
  }

}
