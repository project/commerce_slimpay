<?php

namespace Drupal\commerce_slimpay\Plugin\Commerce\PaymentGateway;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use HapiClient\Exception\HttpException;
use HapiClient\Hal\CustomRel;
use HapiClient\Hal\Resource;
use HapiClient\Http\Auth\Oauth2BasicAuthentication;
use HapiClient\Http\Follow;
use HapiClient\Http\HapiClient;
use HapiClient\Http\JsonBody;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Provides Slimpay's Direct debit gateway.
 *
 * @CommercePaymentGateway(
 *   id = "commerce_slimpay_direct_debit",
 *   label = @Translation("SlimPay (Direct Debit)"),
 *   display_label = @Translation("Direct Debit"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_slimpay\PluginForm\DirectDebitForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "visa", "mastercard", "americanexpress",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class DirectDebit extends OffsitePaymentGatewayBase {

  /**
   * Test API URL.
   */
  const TEST_API_URL = 'https://api.preprod.slimpay.com';

  /**
   * Live API URL.
   */
  const LIVE_API_URL = 'https://api.slimpay.net';

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'app_id' => '',
      'app_secret' => '',
      'creditor' => '',
      'iframe_mode' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App ID'),
      '#default_value' => $this->configuration['app_id'],
      '#required' => TRUE,
    ];
    $form['app_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App Secret'),
      '#default_value' => $this->configuration['app_secret'],
      '#required' => TRUE,
    ];
    $form['creditor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Creditor Reference'),
      '#default_value' => $this->configuration['creditor'],
      '#required' => TRUE,
    ];
    $form['entity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity Reference'),
      '#description' => $this->t("Leave empty if creditor is not configured with entities."),
      '#default_value' => $this->configuration['entity'],
    ];
    $form['iframe_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Iframe mode'),
      '#default_value' => $this->configuration['iframe_mode'],
      '#options' => [
        'iframepopin' => 'Popin',
        'iframeembedded' => 'Embedded',
      ],
      '#required' => TRUE,
    ];
    $form['payment_scheme'] = [
      '#type' => 'radios',
      '#title' => $this->t('paymentScheme'),
      '#default_value' => $this->configuration['payment_scheme'],
      '#options' => [
        'SEPA.DIRECT_DEBIT.CORE' => 'SEPA.DIRECT_DEBIT.CORE',
        'SEPA.DIRECT_DEBIT.B2B' => 'SEPA.DIRECT_DEBIT.B2B',
        'BACS.DIRECT_DEBIT' => 'BACS.DIRECT_DEBIT',
      ],
      '#required' => TRUE,
    ];
    $form['document_template'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Document Template'),
      '#default_value' => $this->configuration['document_template'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    $this->configuration['app_id'] = $values['app_id'];
    $this->configuration['app_secret'] = $values['app_secret'];
    $this->configuration['creditor'] = $values['creditor'];
    $this->configuration['entity'] = $values['entity'];
    $this->configuration['iframe_mode'] = $values['iframe_mode'];
    $this->configuration['payment_scheme'] = $values['payment_scheme'];
    $this->configuration['document_template'] = $values['document_template'];
  }

  /**
   * Gets the client connection to Slimpay API.
   */
  public function getApiClient() {
    return new HapiClient(
      $this->configuration['mode'] === 'live' ? self::LIVE_API_URL : self::TEST_API_URL,
      '/',
      'https://api.slimpay.net/alps/v1',
      new Oauth2BasicAuthentication(
        '/oauth/token',
        $this->configuration['app_id'],
        $this->configuration['app_secret']
      )
    );
  }

  /**
   * Gets iframe for payment's extended approval in Slimpay.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *
   * @return string|false
   *   The iframe code or false on failure.
   */
  public function getPaymentIframe(PaymentInterface $payment) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $payment->getOrder();
    /** @var \Drupal\profile\Entity\ProfileInterface $profile */
    $billing_profile = $order->getBillingProfile();
    /** @var \Drupal\address\AddressInterface $address */
    $address = $billing_profile->address->first();

    $success_url = Url::fromRoute(
      'commerce_slimpay.checkout.success',
      ['commerce_order' => $order->id()],
      ['absolute' => TRUE]
    )->toString();
    $cancel_url = Url::fromRoute(
      'commerce_slimpay.checkout.cancel',
      ['commerce_order' => $order->id()],
      ['absolute' => TRUE]
    )->toString();

    $body = [
      'successUrl' => $success_url,
      'cancelUrl' => $cancel_url,
      'failureUrl' => $cancel_url,
      'started' => 1,
      'paymentScheme' => $this->configuration['payment_scheme'],
      'creditor' => ['reference' => $this->configuration['creditor']],
      'subscriber' => ['reference' => $billing_profile->id()],
      'items' => [
        [
          'type' => 'signMandate',
          'action' => 'sign',
          'autoGenReference' => 1,
          'mandate' => [
            'signatory' => [
              'billingAddress' => [
                'street1' => $address->getAddressLine1(),
                'street2' => $address->getAddressLine2(),
                'city' => $address->getLocality(),
                'postalCode' => $address->getPostalCode(),
                'country' => $address->getCountryCode(),
              ],
              'familyName' => $address->getFamilyName(),
              'givenName' => $address->getGivenName(),
            ],
            'standard' => 'SEPA',
            'paymentScheme' => $this->configuration['payment_scheme'],
          ],
          'signatureApproval' => [
            'method' => ['type' => 'otp'],
          ],
        ],
        [
          'type' => 'templateBasedDocument',
          'action' => 'sign',
          'documentTemplate' => [
            'reference' => $this->configuration['document_template'],
          ],
          'signatory' => [
            'billingAddress' => [
              'street1' => $address->getAddressLine1(),
              'street2' => $address->getAddressLine2(),
              'city' => $address->getLocality(),
              'postalCode' => $address->getPostalCode(),
              'country' => $address->getCountryCode(),
            ],
            'familyName' => $address->getFamilyName(),
            'givenName' => $address->getGivenName(),
          ],
        ],
      ],
    ];

    if ($this->configuration['entity']) {
      $body['creditor']['entity'] = [
        'reference' => $this->configuration['entity'],
      ];
    }

    try {
      $api_client = $this->getApiClient();
      $payment_resource = $api_client->sendFollow(
        new Follow(
          new CustomRel('https://api.slimpay.net/alps#create-orders'),
          'POST',
          NULL,
          new JsonBody($body)
        )
      );
      $extended_approval_resource = $api_client->sendFollow(new Follow(
        new CustomRel('https://api.slimpay.net/alps#extended-user-approval'),
        'GET',
        ['mode' => $this->configuration['iframe_mode']]
      ), $payment_resource);
    }
    catch (HttpException $e) {
      throw new PaymentGatewayException($e->getResponseBody());
    }
    $remote_payment = $payment_resource->getState();
    $payment->setRemoteId($remote_payment['id']);
    $payment->setRemoteState($remote_payment['state']);
    $payment->save();
    return base64_decode($extended_approval_resource->getState()['content']);
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $remote_payment = (Resource::fromJson($request->getContent()))->getState();

    /** @var \Drupal\commerce_payment\PaymentStorage $payment_storage */
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $payment_storage->loadByRemoteId($remote_payment['id']);

    // Only proceed if payment is created.
    if ($payment === NULL) {
      return;
    }

    if (strpos($remote_payment['state'], 'closed.aborted') === 0) {
      $payment_transition = $payment->getState()
        ->getWorkflow()
        ->getTransition('void');
      $payment->getState()->applyTransition($payment_transition);
    }
    elseif (strpos($remote_payment['state'], 'closed.completed') === 0) {
      $payment_transition = $payment->getState()
        ->getWorkflow()
        ->getTransition('authorize_capture');
      $payment->getState()->applyTransition($payment_transition);
    }
    else {
      new BadRequestHttpException(
        "Invalid order state: {$remote_payment['state']}"
      );
    }

    $payment->setRemoteState($remote_payment['state']);
    $payment->save();
  }

}
